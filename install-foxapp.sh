#!/bin/bash -xv

#Verifica se o script está sendo rodado como root/sudo.
if [ "$(id -u)" -ne 0 ] ; then echo "Por favor, execute o script como super-usuário sudo/root." ;exit 1 ; fi 

#Baixar repositório git dos scripts
mkdir -p /srv/git/foxapp_scripts
cd /srv/git/foxapp_scripts 
git init
git remote rm origin
git config --global user.email "you@example.com"
git remote add origin https://gitlab.com/foxmanager/foxmanager-linux-server.git

git checkout master

git reset --hard HEAD
git reset --hard origin/master
git clean -df
git reset --hard master #origin/master
git pull origin master --depth 1 

bash menu.sh
