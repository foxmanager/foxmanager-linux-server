#!/bin/bash -xv

#Carregar variaveis e funções básicas
. fox_environment.aux

#################################
# INÍCIO DO SCRIPT FOXMANAGER
#################################

clear    >&3 

printf "
******************************************
 INSTALAÇÃO FOXMANAGER EM SERVIDOR UBUNTU
******************************************

Sua versão do Ubuntu é:\n\n "  >&3 

lsb_release -d -s  >&3 

#################################
# UTILITÁRIOS E AJUSTES BÁSICOS # 
#################################

printf "
Na próxima etapa, serão instalados e configurados
os seguintes utilitários e ajustes:\n\n
********************************************
- Atualização do Ubuntu (update/upgrade)
- OpenSSH
- Calculadora básica
- Zip e Unzip 
- Ajuste de idioma para pt_BR 
- Ativar o Firewall
********************************************
\n"  >&3 
echo " "
#read -r -p "Pressione enter para continuar ..." key   >&3 


echo Digite o número da opção desejada.  >&3 
echo "[1] Continuar com a instalação básica"  >&3 
echo "[2] Desistir e sair"  >&3 
 
read continuar_basico
	
if [ ! $continuar_basico = "1" ]; then
	exit 1
fi


#Atualizações do Ubuntu
printf "Atualizando Ubuntu ...\n"  >&3   	#Print para a tela
printf "Atualizando Ubuntu ...\n" 		#Print para o log
apt-get -y update
apt-get -y upgrade

#OpenSSH
printf "Instalando OpenSSH ...\n"  >&3 
printf "Instalando OpenSSH ...\n" 
apt-get -y install openssh-server
#Enable SSH idle session timeout of 60 minutes (60 * 60 sec )
echo ClientAliveInterval 60 >> /etc/ssh/sshd_config
echo ClientAliveCountMax 60 >> /etc/ssh/sshd_config
service sshd restart

#Calculadora basica para shell
printf "Instalando Calculadora básica ...\n"  >&3 
printf "Instalando Calculadora básica ...\n" 
apt-get -y install bc

#zip/unzip
printf "Instalando Zip e Unzip ...\n"  >&3 
printf "Instalando Zip e Unzip ...\n" 
apt-get -y install zip unzip

#Ajustes de idioma
printf "Ajustando idioma para pt-BR ...\n"  >&3 
printf "Ajustando idioma para pt-BR ...\n" 
locale-gen pt_BR
update-locale LANG=pt_BR.ISO-8859-1

#Firewall
printf "Configurando Firewall...\n"  >&3 
printf "Configurando Firewall...\n" 
ufw disable
iptables -F
iptables -X
ufw allow 22/tcp #remote connection SSL 
ufw --force enable

##################################
# AJUSTES FINAIS
##################################

printf "Outros ajustes finais ...\n"  >&3 
printf "Outros ajustes finais ...\n" 

#Remove eventuais arquivos de trava
if [ -d "/var/lib/apt/lists/lock" ]; then 	rm -r /var/lib/apt/lists/lock ; fi
if [ -d "/var/lib/dpkg/lock" ]; then rm -r /var/lib/dpkg/lock ; fi
if [ -d "/var/cache/apt/archives/lock" ]; then 	rm -r /var/cache/apt/archives/lock ; fi

#Remover aviso de upgrade de versão do linux
sed -i 's/Prompt=.*/Prompt=never/' /etc/update-manager/release-upgrades
apt-get -y remove ubuntu-release-upgrader-core
chmod -x /etc/update-motd.d/91-release-upgrade

#Limpar pacotes desnecessários	
apt-get -y autoremove
apt-get clean

printf "\n *** Instalação básica concluída ... *** 
\n " >&3


while :
do
    clear
    cat<<EOF >&3
================================= 
  Menu de instalação FoxManager
--------------------------------- 

1) * Java/Tomcat (aplicação) 
2) * FoxManager
3) PostgreSQL (banco de dados) -- Recomenda-se uma instância diferente da aplicação.
4) Pasta Amazon Elastic File System (AWS EFS) 
5) Jenkins
9) Sair 

    ------------------------------ 
Digite o número da aplicação que deseja instalar:
EOF
    read -n1 -s
    case "$REPLY" in
    "1") echo java-tomcat.aux ;;
	"2") bash foxmanager.aux ;;
    "3") . postgresql.aux ;;
    "4") bash efs.aux ;;
    "5") bash jenkins.aux ;;
    "9")  exit                      ;;
    "q")  echo "case sensitive!!"   ;; 
     * )  echo "Opção inválida"     ;;
    esac
    sleep 1
done

